package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class MazeSplit extends Thread {
	private final ArrayList<Point> visited;
	private final Point current;
	private final Point end;
	private final BufferedImage maze;

	public MazeSplit(ArrayList<Point> visited, Point current, Point end, BufferedImage maze){
		this.visited = visited;
		this.current = current;
		this.end = end;
		this.maze = maze;
	}

	@Override
	public void run(){
		if(current.getX() == end.getX() && current.getY() == end.getY()){
			Main.finished(this);

			return;
		}
		ArrayList<Point> nextRound = new ArrayList<>();
		Point point;
		for (int i = 0; i < 4; i++) {
			point = getPoint(i);
			if (point == null) {
				continue;
			} else if (point.x < 0 || point.x > maze.getWidth(null) - 1) {
				continue;
			} else if (point.y < 0 || point.y > maze.getHeight(null) - 1) {
				continue;
			} else if(visited.contains(point)){
				continue;
			}else if(!Main.isWhite(maze.getRGB(point.x, point.y))) {
				continue;
			}
			nextRound.add((Point) point.clone());
		}

		for (Point nextPoint : nextRound) {
			ArrayList<Point> nextPoints = (ArrayList<Point>) visited.clone();
			nextPoints.add(nextPoint);
			MazeSplit newThread = new MazeSplit(nextPoints, nextPoint, end, maze);
			Main.addThread(newThread);
			newThread.start();
		}

		Main.removeThread(this);
	}

	private Point getPoint(int i) {
		switch (i){
			case 0:
				return new Point(current.x, current.y - 1);
			case 1:
				return new Point(current.x, current.y + 1);
			case 2:
				return new Point(current.x - 1, current.y);
			case 3:
				return new Point(current.x + 1, current.y);
		}
		return null;
	}

	public ArrayList<Point> getVisited() {
		return visited;
	}

	public Point getPoint() {
		return current;
	}
}
