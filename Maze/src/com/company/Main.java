package com.company;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Main {

    private static ArrayList<MazeSplit> threads = new ArrayList<>();
    private static MazeSplit winner;
    private static BufferedImage maze;
    private static Point start;
    private static Point end;
    private final static String location = "src/com/company/";
    private final static String name = "download_(4).png";

    private final static Timer timer = new Timer();

    private static long time;

    public static void main(String[] args) {

        time = System.currentTimeMillis();

        try {
            maze = ImageIO.read(new File(location + name));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < maze.getWidth(); i++){
            System.out.println(isWhite(maze.getRGB(i, 0)));
            if(isWhite(maze.getRGB(i, 0))){
                start = new Point(i, 0);
                i = maze.getWidth();
            }
        }
        for(int i = 0; i < maze.getWidth(); i++){
            if(isWhite(maze.getRGB(i, maze.getHeight() - 1))){
                end = new Point(i, maze.getHeight() - 1);
                i = maze.getWidth();
            }
        }
        System.out.println("Start: " + start);
        ArrayList<Point> list = new ArrayList<>();
        list.add(start);
        MazeSplit newThread = new MazeSplit(list, start, end, maze);
        threads.add(newThread);
        newThread.start();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Running " + Thread.getAllStackTraces().keySet().size() + " threads and using " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1000000 + " mb of RAM over " + ((System.currentTimeMillis() - time) / 1000.0) + " seconds");
            }
        }, 0, 500);

    }

    public static void addThread(MazeSplit thread){
        synchronized (threads) {
            threads.add(thread);
        }
    }

    public static synchronized void finished(MazeSplit finals){
        timer.cancel();
        System.out.println("Finished on: " + finals.getPoint() + " in " + ((System.currentTimeMillis() - time) / 1000.0) + " seconds");
        winner = finals;
        synchronized (threads) {
            for (MazeSplit split : threads) {
                if (split.isAlive() && split != finals) {
                    split.stop();
                }
            }
        }
        for(Point point : winner.getVisited()){
            maze.setRGB(point.x, point.y, 128);
        }
        try {
            String newFile = location + (name.replaceAll(".png", "") + "_solved.png");
            File newImgFile = new File(newFile);
            ImageIO.write(maze, "PNG", newImgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isWhite(int pixel){
        int red = (pixel >> 16) & 0xff;
        int green = (pixel >> 8) & 0xff;
        int blue = (pixel) & 0xff;
        return red == 255 && green == 255 && blue == 255;
    }


    public static void removeThread(MazeSplit mazeSplit) {
        synchronized (threads){
            threads.remove(mazeSplit);
        }
    }
}
